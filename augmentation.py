from matplotlib.colors import NoNorm
import matplotlib.pyplot as plt
from skimage import transform
from scipy import ndarray
from skimage import util
from PIL import Image
from skimage import filters
from skimage import io
import skimage as sk
import numpy as np
import skimage
import random
import glob
import sys
import cv2
import os


# inputs:
# Image path
# images_path = r'C:\Users\or.zilberberg\Desktop\Or\proj\Data'
images_path = r'C:\Users\uri.cohn\OneDrive - Bright Machines\FRC\Vision\Lulavi Project\WonderWoman\renderOutputs\White1600umOpen'
# image path
folder_path = images_path
# number of images to generate: 0 - all else #num
num_files_desired = 0

random_flag = 1            # 1 - random transformations actions; 0 - one random transformations action
gray_flag = 1              # 1 - color images 0 - gray images
demonstration = 0

# Transformations inputs:
deg = 5                    # rotation ± degrees
noise_type = 'gaussian'    # localvar, s&p, speckle
noise_amount = 5           # defult is 10
sigma = 1.3                # gauss param
                           # 'reflect', 'constant', 'nearest', 'mirror', 'wrap'

min_gamma_b = 0.4
max_gamma_b = 0.9
min_gamma_d = 1.2
max_gamma_d = 2


def get_image_data(images_folder_path):
    """
    The functions read file from a given directory path, converts color to gray and returns a dictionary with images data
    :param images_folder_path: images folder location
    :return: dictionary with file name as a key and cv2 object image as value
    """
    images_data_dict = {}
    try:
        full_path = os.path.join(images_folder_path, '*g')
        all_images = glob.glob(full_path)
        for image in all_images:
            if os.path.isfile(image):
                file_name = os.path.basename(image)
                read_img = cv2.imread(image)
                gray_img = cv2.cvtColor(read_img, cv2.COLOR_BGR2GRAY)
                images_data_dict[file_name] = gray_img
        return images_data_dict
    except Exception as e:
        print("Unable to read imges: {e.msg}")


def random_rotation(image_array: ndarray):
    # pick a random degree of rotation between 25% on the left and 25% on the right
    random_degree = random.uniform(-deg, deg)
    return sk.transform.rotate(image_array, random_degree)


def random_noise(image_array: ndarray):
    # add random noise to the image
    return sk.util.random_noise(image_array, noise_type, noise_amount)


def horizontal_flip(image_array: ndarray):
    # horizontal flip doesn't need skimage, flipping the image array of pixels !
    return image_array[::-1, :]


def vertical_flip(image_array: ndarray):
    # vertical flip doesn't need skimage,flipping the image array of pixels !
    return image_array[:, ::-1]


def gauss(image_array: ndarray):
    # guass: standard deviation for Gaussian kernel
    image = image_array
    return sk.filters.gaussian(image, sigma=sigma)


def change_contrast_bright(img):
    gain = 1
    random_gamma = random.uniform(min_gamma_b, max_gamma_b)
    return skimage.exposure.adjust_gamma(img, random_gamma, gain)


def change_contrast_dark(img):
    gain = 1
    random_gamma = random.uniform(min_gamma_d, max_gamma_d)
    return skimage.exposure.adjust_gamma(img, random_gamma, gain)


def transform_im(transformations, image_to_transform, num_generated_files, image_path, new_path, origin_new_path, image_name):
    transformations_2 = transformations
    num_transformations = 0
    while num_transformations <= num_transformations_to_apply-1:
        # random transformation to apply for a single image
        key = random.choice(list(transformations_2))
        print(key)
        transformed_image = transformations_2[key](image_to_transform)
        del transformations_2[key]

        im_name = image_path.split('\\')[-1].split('.')[0]
        # new_file_path = '%s/%s_%s_%s.jpg' % (new_path, im_name, num_generated_files, num_transformations)
        if random_flag:
            new_file_path = '%s/%s_%s_%s.jpg' % (new_path, im_name, num_generated_files, num_transformations)
        else:
            new_file_path = '%s/%s.jpg' % (new_path, image_name)

        # write image to the disk
        sk.io.imsave(new_file_path, transformed_image)
        if gray_flag:
            origin_g_file_path = '%s/%s.jpg' % (origin_new_path, image_name)
            # origin_g_file_path = '%s/%s_%s_%s_origin.jpg' % (origin_new_path, im_name, num_generated_files, num_transformations)
            sk.io.imsave(origin_g_file_path, image_to_transform)
        num_transformations += 1


# dictionary of the transformations
available_transformations = {
    'rotate': random_rotation,
    'bright_contrast': change_contrast_bright,
    'dark_contrast': change_contrast_dark,
    'noise': random_noise,
    'gauss': gauss,
#    'horizontal_flip': horizontal_flip,
#    'vertical_flip': vertical_flip
}
# loop on all files of the folder and build a list of files paths
images = [os.path.join(folder_path, f) for f in os.listdir(folder_path) if os.path.isfile(os.path.join(folder_path, f))]

if num_files_desired == 0:
    num_files_desired = len(images)

# create folders for new images
new_path = folder_path + '\\augment\\'
origin_new_path = folder_path + '\\origin\\'
if not os.path.exists(new_path):
    os.makedirs(new_path)
if not os.path.exists(origin_new_path):
    os.makedirs(origin_new_path)


num_generated_files = 0
while num_generated_files <= num_files_desired-1:
    print("image: {}".format(num_generated_files))
    # random image from the folder
    image_path = images[num_generated_files]
    image_name = images[num_generated_files].split('\\')[-1].split('.')[0]
    # read image as an two dimensional array of pixels as color or gray
    if gray_flag == 1:
        image_to_transform = cv2.cvtColor(sk.io.imread(image_path), cv2.COLOR_BGR2GRAY)
    else:
        image_to_transform = sk.io.imread(image_path)

    # random transformations or just 1 per image
    if random_flag == 1:
        num_transformations_to_apply = random.randint(1, len(available_transformations))
    else:
        num_transformations_to_apply = 1

    transform_im(available_transformations.copy(), image_to_transform, num_generated_files, image_path, new_path, origin_new_path, image_name)
    num_generated_files += 1


if demonstration == 1:
    path = r'C:\Users\or.zilberberg\Desktop\Or\proj\Data\demonstration'
    im_1_path = r'\augmented_image_0_0.jpg'
    im_2_path = r'\augmented_image_0_1.jpg'
    im_3_path = r'\augmented_image_0_2.jpg'
    im_4_path = r'\augmented_image_0_3.jpg'
    im_5_path = r'\augmented_image_0_4.jpg'
    im_6_path = r'\augmented_image_0_5.jpg'
    im_7_path = r'\augmented_image_0_6.jpg'
    im_origin_path = r'\origin.bmp'

    im_1 = cv2.cv2.imread(path+im_1_path)
    im_2 = cv2.cv2.imread(path+im_2_path)
    im_3 = cv2.cv2.imread(path+im_3_path)
    im_4 = cv2.cv2.imread(path+im_4_path)
    im_5 = cv2.cv2.imread(path+im_5_path)
    im_6 = cv2.cv2.imread(path+im_6_path)
    im_7 = cv2.cv2.imread(path+im_7_path)
    im_origin = cv2.cv2.imread(path+im_origin_path)

    plt.figure(figsize=(18, 13))
    # plt.figure(1)
    plt.subplot(241)
    plt.title('Origin image')
    plt.imshow(im_origin)
    plt.subplot(242)
    plt.title('Noise image')
    plt.imshow(im_1)
    plt.subplot(243)
    plt.title('Bright contrast image')
    plt.imshow(im_2)
    plt.subplot(244)
    plt.title('Vertical flip image')
    plt.imshow(im_3)
    plt.subplot(245)
    plt.title('Gauss image')
    plt.imshow(im_4)
    plt.subplot(246)
    plt.title('Rotate image')
    plt.imshow(im_5)
    plt.subplot(247)
    plt.title('Dark contrast image')
    plt.imshow(im_6)
    plt.subplot(248)
    plt.title('Horizontal flip image')
    plt.imshow(im_7)

    mng = plt.get_current_fig_manager()
    mng.full_screen_toggle()
    plt.show()